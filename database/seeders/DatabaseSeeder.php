<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Reimbursement;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();
        $this->call(RolesAndPermissionsSeeder::class);
        //make user DIREKTUR
        User::factory()->create([
            'nama' => 'DONI',
            'nip' => '1234',
        ])->assignRole('DIREKTUR');
        //make user FINANCE
        User::factory()->create([
            'nama' => 'DONO',
            'nip' => '1235',
        ])->assignRole('FINANCE');
        //make user STAFF
        User::factory()->create([
            'nama' => 'DONA',
            'nip' => '1236',
        ])->assignRole('STAFF');

        User::factory(2)->create()->each(function ($user) {
            $user->assignRole('FINANCE');
        });

        User::factory(30)->has(Reimbursement::factory()->count(1))->create()->each(function ($user) {
            $user->assignRole('STAFF');
        });
    }
}
