<?php

namespace Database\Seeders;

use App\Enums\JabatanEnum;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $roles = JabatanEnum::getValues();
        foreach ($roles as $role) {
            $permissions = JabatanEnum::getPermissionByJabatan($role);
            //create permission
            foreach ($permissions as $permission) {
                Permission::firstOrCreate(['name' => $permission]);
            }
            //create role
            Role::create(['name' => $role])->givePermissionTo($permissions);
        }
    }
}
