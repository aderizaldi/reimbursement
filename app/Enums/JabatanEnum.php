<?php

declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

final class JabatanEnum extends Enum
{
    const Direktur = 'DIREKTUR';
    const Finance = 'FINANCE';
    const Staff = 'STAFF';

    //make array permission by jabatan
    public static function getPermissionByJabatan($jabatan)
    {
        $permission = [];
        switch ($jabatan) {
            case self::Direktur:
                $permission = [
                    'buat karyawan',
                    'ubah karyawan',
                    'hapus karyawan',
                    'terima pengajuan reimbursement',
                    'tolak pengajuan reimbursement',
                ];
                break;
            case self::Finance:
                $permission = [
                    'terima pengajuan reimbursement',
                    'tolak pengajuan reimbursement',
                ];
                break;
            case self::Staff:
                $permission = [
                    'pengajuan reimbursement',
                ];
                break;
        }
        return $permission;
    }

    //get values except Direktur
    public static function getArrayExceptDirektur()
    {
        //return array values except Direktur
        return [
            'Finance' => self::Finance,
            'Staff' => self::Staff,
        ];
    }

    //get values except Direktur
    public static function getValuesExceptDirektur()
    {
        //return array values except Direktur
        return [
            self::Finance,
            self::Staff,
        ];
    }
}
