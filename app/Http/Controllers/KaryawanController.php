<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Enums\JabatanEnum;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class KaryawanController extends Controller
{
    public function karyawan()
    {
        //cek request ajax atau bukan
        if (request()->ajax()) {
            $karyawan = User::latest()->withoutRole('DIREKTUR');
            return datatables()->of($karyawan)
                ->addIndexColumn()
                ->addColumn('jabatan', function ($karyawan) {
                    return $karyawan->getRoleNames()->first();
                })
                ->addColumn('action', function ($karyawan) {
                    return '<button class="btn btn-sm btn-warning edit-btn" data-bs-toggle="modal" data-bs-target="#modalEdit" data-id="' . $karyawan->id . '" data-nama="' . $karyawan->nama . '" data-nip="' . $karyawan->nip . '" data-jabatan="' . $karyawan->getRoleNames()->first() . '"><i class="tf-icons mdi mdi-square-edit-outline"></i></button>
                    <button class="btn btn-sm btn-danger delete-btn" data-bs-toggle="modal" data-bs-target="#modalDelete" data-id="' . $karyawan->id . '"><i class="tf-icons mdi mdi-delete-empty-outline"></i></button>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('user.pages.karyawan');
    }

    public function karyawanPost(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'nip' => 'required|unique:users,nip',
            'password' => 'required|min:4',
            'jabatan' => 'required|in:' . implode(',', JabatanEnum::getValuesExceptDirektur()),
        ]);

        $user = new User();
        $user->nama = $request->nama;
        $user->nip = $request->nip;
        $user->password = bcrypt($request->password);
        $user->save();

        $user->assignRole($request->jabatan);

        return back()->with('success', 'Karyawan berhasil ditambahkan');
    }

    public function karyawanPut(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|string|max:255',
            'nip' => 'required|unique:users,nip,' . $id,
            'jabatan' => 'required|in:' . implode(',', JabatanEnum::getValuesExceptDirektur()),
            'password' => 'nullable|string|min:4',
        ]);

        $user = User::find($id);
        $user->nama = $request->nama;
        $user->nip = $request->nip;
        if ($request->password) $user->password = bcrypt($request->password);
        $user->save();

        $user->syncRoles([$request->jabatan]);

        return back()->with('success', 'Karyawan berhasil diubah');
    }

    public function karyawanDelete($id)
    {
        $user = User::find($id);
        $user->delete();

        return back()->with('success', 'Karyawan berhasil dihapus');
    }
}
