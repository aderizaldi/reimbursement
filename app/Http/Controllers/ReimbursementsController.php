<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reimbursement;

class ReimbursementsController extends Controller
{
    public function pengajuanReimbursement()
    {
        if (request()->ajax()) {
            $reimbursements = Reimbursement::where('user_id', auth()->id())->latest();
            return datatables()->of($reimbursements)
                ->addIndexColumn()
                ->addColumn('status_direktur', function ($reimbursement) {
                    $status = "<span class='badge bg-warning'>Pending</span>";
                    if ($reimbursement->statusDirektur()?->status === true) {
                        $status = "<span class='badge bg-success'>Diterima</span>";
                    } elseif ($reimbursement->statusDirektur()?->status === false) {
                        $status = "<span class='badge bg-danger'>Ditolak</span>";
                    }
                    return $status;
                })
                ->addColumn('status_finance', function ($reimbursement) {
                    $status = "<span class='badge bg-warning'>Pending</span>";
                    if ($reimbursement->statusFinance()?->status === true) {
                        $status = "<span class='badge bg-success'>Diterima</span>";
                    } elseif ($reimbursement->statusFinance()?->status === false) {
                        $status = "<span class='badge bg-danger'>Ditolak</span>";
                    }
                    return $status;
                })
                ->editColumn('bukti', function ($reimbursement) {
                    $url = filter_var($reimbursement->bukti, FILTER_VALIDATE_URL) ? $reimbursement->bukti : asset('storage/' . $reimbursement->bukti);
                    return '<a class="btn btn-secondary btn-sm" href="' . $url . '" target="_blank"><i class="mdi mdi-eye-circle-outline me-1"></i>Lihat</a>';
                })
                ->editColumn('deskripsi', function ($reimbursement) {
                    return $reimbursement->deskripsi ?? '-';
                })
                ->editColumn('tanggal', function ($reimbursement) {
                    return $reimbursement->tanggal->format('d F Y');
                })
                ->rawColumns(['bukti', 'status_direktur', 'status_finance'])
                ->make(true);
        }
        return view('user.pages.pengajuan_reimbursement');
    }

    public function reimbursementDirektur()
    {
        if (request()->ajax()) {
            $reimbursements = Reimbursement::whereDoesntHave('terimaTolakReimbursements', function ($query) {
                $query->whereRelation('user', function ($user) {
                    $user->whereHas('roles', function ($query) {
                        $query->where('name', 'DIREKTUR');
                    });
                });
            })->latest();
            return datatables()->of($reimbursements)
                ->addIndexColumn()
                ->editColumn('bukti', function ($reimbursement) {
                    $url = filter_var($reimbursement->bukti, FILTER_VALIDATE_URL) ? $reimbursement->bukti : asset('storage/' . $reimbursement->bukti);
                    return '<a class="btn btn-secondary btn-sm" href="' . $url . '" target="_blank"><i class="mdi mdi-eye-circle-outline me-1"></i>Lihat</a>';
                })
                ->editColumn('deskripsi', function ($reimbursement) {
                    return $reimbursement->deskripsi ?? '-';
                })
                ->editColumn('tanggal', function ($reimbursement) {
                    return $reimbursement->tanggal->format('d F Y');
                })
                ->addColumn('action', function ($reimbursement) {
                    return '<button class="btn btn-success btn-sm btn-terima" data-bs-toggle="modal" data-bs-target="#terimaModal" data-id="' . $reimbursement->id . '"><i class="tf-icons mdi mdi-check me-1"></i></button>
                    <button class="btn btn-danger btn-sm btn-tolak" data-bs-toggle="modal" data-bs-target="#tolakModal" data-id="' . $reimbursement->id . '"><i class="tf-icons mdi mdi-close me-1"></i></button>';
                })
                ->addColumn('pengaju', function ($reimbursement) {
                    return $reimbursement->user->nama . ' (' . $reimbursement->user->nip . ')';
                })
                ->rawColumns(['action', 'bukti'])
                ->make(true);
        }
        return view('user.pages.reimbursement_direktur');
    }

    public function historyReimbursementDirektur()
    {
        if (request()->ajax()) {
            $reimbursements = Reimbursement::whereHas('terimaTolakReimbursements', function ($query) {
                $query->whereRelation('user', function ($user) {
                    $user->whereHas('roles', function ($query) {
                        $query->where('name', 'DIREKTUR');
                    });
                });
            })->orderBy('updated_at', 'desc')->get();
            return datatables()->of($reimbursements)
                ->addIndexColumn()
                ->editColumn('bukti', function ($reimbursement) {
                    $url = filter_var($reimbursement->bukti, FILTER_VALIDATE_URL) ? $reimbursement->bukti : asset('storage/' . $reimbursement->bukti);
                    return '<a class="btn btn-secondary btn-sm" href="' . $url . '" target="_blank"><i class="mdi mdi-eye-circle-outline me-1"></i>Lihat</a>';
                })
                ->editColumn('deskripsi', function ($reimbursement) {
                    return $reimbursement->deskripsi ?? '-';
                })
                ->editColumn('tanggal', function ($reimbursement) {
                    return $reimbursement->tanggal->format('d F Y');
                })
                ->addColumn('status_direktur', function ($reimbursement) {
                    $status = "<span class='badge bg-warning'>Pending</span>";
                    if ($reimbursement->statusDirektur()?->status === true) {
                        $status = "<span class='badge bg-success'>Diterima</span>";
                    } elseif ($reimbursement->statusDirektur()?->status === false) {
                        $status = "<span class='badge bg-danger'>Ditolak</span>";
                    }
                    return $status;
                })
                ->addColumn('status_finance', function ($reimbursement) {
                    $status = "<span class='badge bg-warning'>Pending</span>";
                    if ($reimbursement->statusFinance()?->status === true) {
                        $status = "<span class='badge bg-success'>Diterima</span>";
                    } elseif ($reimbursement->statusFinance()?->status === false) {
                        $status = "<span class='badge bg-danger'>Ditolak</span>";
                    }
                    return $status;
                })
                ->addColumn('pengaju', function ($reimbursement) {
                    return $reimbursement->user->nama . ' (' . $reimbursement->user->nip . ')';
                })
                ->rawColumns(['bukti', 'status_direktur', 'status_finance'])
                ->make(true);
        }
        return view('user.pages.history_reimbursement_direktur');
    }

    public function reimbursementFinance()
    {
        if (request()->ajax()) {
            $reimbursements = Reimbursement::whereDoesntHave('terimaTolakReimbursements', function ($query) {
                $query->whereRelation('user', function ($user) {
                    $user->whereHas('roles', function ($query) {
                        $query->where('name', 'FINANCE');
                    });
                });
            })->whereHas('terimaTolakReimbursements', function ($query) {
                $query->whereRelation('user', function ($user) {
                    $user->whereHas('roles', function ($query) {
                        $query->where('name', 'DIREKTUR');
                    })->where('status', true);
                });
            })->latest();
            return datatables()->of($reimbursements)
                ->addIndexColumn()
                ->editColumn('bukti', function ($reimbursement) {
                    $url = filter_var($reimbursement->bukti, FILTER_VALIDATE_URL) ? $reimbursement->bukti : asset('storage/' . $reimbursement->bukti);
                    return '<a class="btn btn-secondary btn-sm" href="' . $url . '" target="_blank"><i class="mdi mdi-eye-circle-outline me-1"></i>Lihat</a>';
                })
                ->editColumn('deskripsi', function ($reimbursement) {
                    return $reimbursement->deskripsi ?? '-';
                })
                ->editColumn('tanggal', function ($reimbursement) {
                    return $reimbursement->tanggal->format('d F Y');
                })
                ->addColumn('action', function ($reimbursement) {
                    return '<button class="btn btn-success btn-sm btn-terima" data-bs-toggle="modal" data-bs-target="#terimaModal" data-id="' . $reimbursement->id . '"><i class="tf-icons mdi mdi-check me-1"></i></button>
                    <button class="btn btn-danger btn-sm btn-tolak" data-bs-toggle="modal" data-bs-target="#tolakModal" data-id="' . $reimbursement->id . '"><i class="tf-icons mdi mdi-close me-1"></i></button>';
                })
                ->addColumn('pengaju', function ($reimbursement) {
                    return $reimbursement->user->nama . ' (' . $reimbursement->user->nip . ')';
                })
                ->rawColumns(['action', 'bukti'])
                ->make(true);
        }
        return view('user.pages.reimbursement_finance');
    }

    public function historyReimbursementFinance()
    {
        if (request()->ajax()) {
            $reimbursements = Reimbursement::whereHas('terimaTolakReimbursements', function ($query) {
                $query->whereRelation('user', function ($user) {
                    $user->whereHas('roles', function ($query) {
                        $query->where('name', 'FINANCE');
                    });
                });
            })->orderBy('updated_at', 'desc')->get();
            return datatables()->of($reimbursements)
                ->addIndexColumn()
                ->editColumn('bukti', function ($reimbursement) {
                    $url = filter_var($reimbursement->bukti, FILTER_VALIDATE_URL) ? $reimbursement->bukti : asset('storage/' . $reimbursement->bukti);
                    return '<a class="btn btn-secondary btn-sm" href="' . $url . '" target="_blank"><i class="mdi mdi-eye-circle-outline me-1"></i>Lihat</a>';
                })
                ->editColumn('deskripsi', function ($reimbursement) {
                    return $reimbursement->deskripsi ?? '-';
                })
                ->editColumn('tanggal', function ($reimbursement) {
                    return $reimbursement->tanggal->format('d F Y');
                })
                ->addColumn('status_direktur', function ($reimbursement) {
                    $status = "<span class='badge bg-warning'>Pending</span>";
                    if ($reimbursement->statusDirektur()?->status === true) {
                        $status = "<span class='badge bg-success'>Diterima</span>";
                    } elseif ($reimbursement->statusDirektur()?->status === false) {
                        $status = "<span class='badge bg-danger'>Ditolak</span>";
                    }
                    return $status;
                })
                ->addColumn('status_finance', function ($reimbursement) {
                    $status = "<span class='badge bg-warning'>Pending</span>";
                    if ($reimbursement->statusFinance()?->status === true) {
                        $status = "<span class='badge bg-success'>Diterima</span>";
                    } elseif ($reimbursement->statusFinance()?->status === false) {
                        $status = "<span class='badge bg-danger'>Ditolak</span>";
                    }
                    return $status;
                })
                ->addColumn('pengaju', function ($reimbursement) {
                    return $reimbursement->user->nama . ' (' . $reimbursement->user->nip . ')';
                })
                ->rawColumns(['bukti', 'status_direktur', 'status_finance'])
                ->make(true);
        }
        return view('user.pages.history_reimbursement_finance');
    }

    public function pengajuanReimbursementPost(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'tanggal' => 'required',
            'bukti' => 'required|file|mimes:jpg,jpeg,png,pdf|max:2048',
            'deskripsi' => 'nullable',
        ]);

        Reimbursement::create([
            'user_id' => auth()->id(),
            'nama' => $request->nama,
            'tanggal' => $request->tanggal,
            'bukti' => $request->file('bukti')->store('reimbursements', 'public'),
            'deskripsi' => $request->deskripsi,
        ]);

        return redirect()->back()->with('success', 'Reimbursement berhasil diajukan');
    }

    public function terimaReimbursement($id)
    {
        $reimbursement = Reimbursement::findOrfail($id);
        $reimbursement->terimaTolakReimbursements()->create([
            'user_id' => auth()->user()->id,
            'status' => 1,
        ]);

        return back()->with('success', 'Reimbursement berhasil diterima');
    }

    public function tolakReimbursement($id)
    {
        $reimbursement = Reimbursement::findOrfail($id);
        $reimbursement->terimaTolakReimbursements()->create([
            'user_id' => auth()->user()->id,
            'status' => 0,
        ]);

        return back()->with('success', 'Reimbursement berhasil ditolak');
    }
}
