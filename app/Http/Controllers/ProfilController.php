<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ProfilController extends Controller
{
    //profile page
    public function profil()
    {
        $user = User::find(auth()->user()->id);
        return view('user.pages.profil', compact('user'));
    }

    //profile post
    public function profilPut(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'nip' => 'required|unique:users,nip,' . auth()->user()->id,
        ]);

        $user = User::find(auth()->user()->id);
        $user->name = $request->name;
        $user->nip = $request->nip;
        $user->save();

        return back()->with('success', 'Profile berhasil diubah');
    }

    //change password post
    public function passwordPut(Request $request)
    {
        $request->validate([
            'password_lama' => 'required',
            'passowrd_baru' => 'required',
        ]);

        $user = User::find(auth()->user()->id);

        if (!password_verify($request->password_lama, $user->password_baru)) {
            return back()->with('error', 'Password lama salah!');
        }

        $user->password = bcrypt($request->password_baru);
        $user->save();

        return back()->with('success', 'Password berhasil diubah');
    }
}
