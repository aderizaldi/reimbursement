<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //login page
    public function login()
    {
        return view('auth.pages.login');
    }

    //login post
    public function loginPost(Request $request)
    {
        //validate request
        $request->validate([
            'nip' => 'required',
            'password' => 'required',
            'remember' => 'nullable',
        ]);

        //attempt login
        if (Auth::attempt($request->only('nip', 'password'), $request->remember ? true : false)) {
            //redirect to home
            return redirect()->route('dashboard');
        }

        //redirect back
        return back()->with('error', 'NIP atau password salah!')->withInput();
    }

    //logout
    public function logout()
    {
        //logout user
        Auth::logout();

        //redirect to login
        return redirect()->route('login');
    }
}
