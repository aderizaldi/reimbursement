<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reimbursement extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'tanggal' => 'date'
    ];

    protected $touches = ['terimaTolakReimbursements'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function terimaTolakReimbursements()
    {
        return $this->hasMany(TerimaTolakReimbursement::class);
    }

    public function statusDirektur()
    {
        return $this->terimaTolakReimbursements()->whereRelation('user', function ($user) {
            return $user->whereHas('roles', function ($query) {
                $query->where('name', 'DIREKTUR');
            });
        })->first();
    }

    public function statusFinance()
    {
        return $this->terimaTolakReimbursements()->whereRelation('user', function ($user) {
            return $user->whereHas('roles', function ($query) {
                $query->where('name', 'FINANCE');
            });
        })->first();
    }
}
