<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\KaryawanController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ReimbursementsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware('guest')->group(function () {
    Route::get('login', [AuthController::class, 'login'])->name('login');
    Route::post('login', [AuthController::class, 'loginPost'])->name('login.post');
});

Route::middleware('auth')->group(function () {
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout.post');
});

Route::middleware('auth')->group(function () {
    Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
    Route::get('profil', [ProfilController::class, 'profil'])->name('profil');
    Route::put('profil', [ProfilController::class, 'profilPut'])->name('profil.put');
    Route::put('profil/password', [ProfilController::class, 'passwordPut'])->name('profil.password.put');

    Route::middleware('role:STAFF')->group(function () {
        Route::get('pengajuan-reimbursement', [ReimbursementsController::class, 'pengajuanReimbursement'])->name('pengajuan-reimbursement');
        Route::post('pengajuan-reimbursement', [ReimbursementsController::class, 'pengajuanReimbursementPost'])->name('pengajuan-reimbursement.post');
    });

    Route::middleware('role:DIREKTUR')->group(function () {
        Route::get('karyawan', [KaryawanController::class, 'karyawan'])->name('karyawan');
        Route::post('karyawan', [KaryawanController::class, 'karyawanPost'])->name('karyawan.post');
        Route::put('karyawan/{id}', [KaryawanController::class, 'karyawanPut'])->name('karyawan.put');
        Route::delete('karyawan{id}', [KaryawanController::class, 'karyawanDelete'])->name('karyawan.delete');

        Route::get('reimbursement-direktur', [ReimbursementsController::class, 'reimbursementDirektur'])->name('reimbursement-direktur');

        Route::get('/history-reimbursement-direktur', [ReimbursementsController::class, 'historyReimbursementDirektur'])->name('history-reimbursement-direktur');
    });

    Route::middleware('role:FINANCE')->group(function () {
        Route::get('reimbursement-finance', [ReimbursementsController::class, 'reimbursementFinance'])->name('reimbursement-finance');
        Route::get('history-reimbursement-finance', [ReimbursementsController::class, 'historyReimbursementFinance'])->name('history-reimbursement-finance');
    });

    Route::prefix('reimbursement')->group(function () {
        Route::put('/terima/{id}', [ReimbursementsController::class, 'terimaReimbursement'])->middleware('can:terima pengajuan reimbursement')->name('reimbursement.terima');
        Route::put('/tolak/{id}', [ReimbursementsController::class, 'tolakReimbursement'])->middleware('can:tolak pengajuan reimbursement')->name('reimbursement.tolak');
    });
});
