# Reimbursement Website

This project created using framework Laravel 10.

## Prerequisites

Make sure you have the following installed on your system:

- PHP >= 8.0
- Composer
- Git

## Installation
### Clone the project
Clone the project using git

```bash
  git clone https://gitlab.com/aderizaldi/reimbursement.git
```

Go to the project directory

```bash
  cd reimbursement
```

### Set up environment variables

Duplicate the .env.example file and rename it to .env. Update the necessary environment variables like database credentials, etc.

### Install depedencies and setup the project
Install dependencies

```bash
  composer install
```

Generate key

```bash
  php artisan key:generate
```

Create symbolic link to storage

```bash
  php artisan storage:link
```

Cretate database and seeder
```bash
  php artisan migrate --seed
```

### Run the project locally

Start the server

```bash
  php artisan serve
```
