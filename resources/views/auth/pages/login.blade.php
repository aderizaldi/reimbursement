@extends('auth.layouts.main')
@section('content')
    <div class="authentication-inner row m-0">
        <!-- /Left Section -->
        <div class="d-none d-lg-flex col-lg-7 col-xl-8 align-items-center justify-content-center p-5 pb-2">
            <div>
                <img src="{{ asset('assets/img/illustrations/boy-with-rocket-light.png') }}"
                    class="authentication-image-model d-none d-lg-block" alt="auth-model">
            </div>
        </div>
        <!-- /Left Section -->

        <!-- Login -->
        <div
            class="d-flex col-12 col-lg-5 col-xl-4 align-items-center authentication-bg position-relative py-sm-5 px-4 py-4">
            <div class="w-px-400 mx-auto pt-5 pt-lg-0">
                <h4 class="mb-2">Selamat datang! 👋</h4>
                <p class="mb-4">Silahkan masuk menggunakan nip dan password terdaftar anda!</p>

                <form class="mb-3" action="{{ route('login.post') }}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="form-floating form-floating-outline mb-3">
                        <input type="text" class="form-control" id="nip" name="nip" placeholder="Masukkan nip"
                            autofocus value="{{ old('nip') }}" required>
                        <label for="nip">Nip</label>
                    </div>
                    <div class="mb-3">
                        <div class="form-password-toggle">
                            <div class="input-group input-group-merge">
                                <div class="form-floating form-floating-outline">
                                    <input type="password" id="password" class="form-control" name="password"
                                        placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                                        aria-describedby="password" required />
                                    <label for="password">Password</label>
                                </div>
                                <span class="input-group-text cursor-pointer"><i class="mdi mdi-eye-off-outline"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3 d-flex justify-content-between flex-wrap">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="remember">
                            <label class="form-check-label me-2" for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary d-grid w-100">
                        Masuk
                    </button>
                </form>
            </div>
        </div>
        <!-- /Login -->
    </div>
@endsection
