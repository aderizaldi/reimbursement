<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo ">
        <a href="#" class="app-brand-link">
            <span class="app-brand-text demo menu-text fw-semibold ms-2">Reimbursement</span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="mdi menu-toggle-icon d-xl-block align-middle mdi-20px"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <li class="menu-item {{ request()->is('dashboard*') ? 'active' : '' }}">
            <a href="{{ route('dashboard') }}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-view-dashboard-outline"></i>
                <div>Dashboard</div>
            </a>
        </li>
        @role('DIREKTUR')
            <li class="menu-item {{ request()->is('reimbursement-direktur*') ? 'active' : '' }}">
                <a href="{{ route('reimbursement-direktur') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-cash-multiple"></i>
                    <div>Reimbursement</div>
                </a>
            </li>
            <li class="menu-item {{ request()->is('history-reimbursement-direktur*') ? 'active' : '' }}">
                <a href="{{ route('history-reimbursement-direktur') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-history"></i>
                    <div>History</div>
                </a>
            </li>
            <li class="menu-item {{ request()->is('karyawan*') ? 'active' : '' }}">
                <a href="{{ route('karyawan') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-account-group-outline"></i>
                    <div>Karyawan</div>
                </a>
            </li>
        @endrole
        @role('STAFF')
            <li class="menu-item {{ request()->is('pengajuan-reimbursement*') ? 'active' : '' }}">
                <a href="{{ route('pengajuan-reimbursement') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-cash-multiple"></i>
                    <div>Pengajuan</div>
                </a>
            </li>
        @endrole
        @role('FINANCE')
            <li class="menu-item {{ request()->is('reimbursement-finance*') ? 'active' : '' }}">
                <a href="{{ route('reimbursement-finance') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-cash-multiple"></i>
                    <div>Reimbursement</div>
                </a>
            </li>
            <li class="menu-item {{ request()->is('history-reimbursement-finance*') ? 'active' : '' }}">
                <a href="{{ route('history-reimbursement-finance') }}" class="menu-link">
                    <i class="menu-icon tf-icons mdi mdi-history"></i>
                    <div>History</div>
                </a>
            </li>
        @endrole
        <li class="menu-header fw-medium mt-5">
            <span class="menu-header-text">Pengaturan</span>
        </li>
        <li class="menu-item {{ request()->is('profil*') ? 'active' : '' }}">
            <a href="{{ route('profil') }}" class="menu-link">
                <i class="menu-icon tf-icons mdi mdi-account-outline"></i>
                <div>Profile</div>
            </a>
        </li>
        <li class="menu-item">
            <a href="#" class="menu-link text-danger" data-bs-toggle="modal" data-bs-target="#logoutModal">
                <i class="menu-icon tf-icons mdi mdi-logout"></i>
                <div>Logout</div>
            </a>
        </li>
</aside>
