@extends('user.layouts.main')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">History Pengajuan Reimbursement</h5>
                    </div>
                    <div class="card-datatable table-responsive pt-0">
                        <table id="datatable-history-reimbursement-finance" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pengaju</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Deskripsi</th>
                                    <th>Bukti</th>
                                    <th>Status Direktur</th>
                                    <th>Status Finance</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#datatable-history-reimbursement-finance').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: "{{ route('history-reimbursement-finance') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        width: '30'
                    },
                    {
                        data: 'pengaju',
                        name: 'pengaju',
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi',
                    },
                    {
                        data: 'bukti',
                        name: 'bukti',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'status_direktur',
                        name: 'status_direktur',
                    },
                    {
                        data: 'status_finance',
                        name: 'status_finance',
                    }
                ],
            });
        });
    </script>
@endpush
