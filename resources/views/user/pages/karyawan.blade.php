@extends('user.layouts.main')
@use('\App\Enums\JabatanEnum')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">Data Karyawan</h5>
                        <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" data-bs-toggle="modal"
                            data-bs-target="#modalTambah"><span class="tf-icons mdi mdi-plus"></span> Karyawan</button>
                    </div>
                    <div class="card-datatable table-responsive pt-0">
                        <table id="datatable-karyawan" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Jabatan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal tambah karyawan --}}
    <div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="tambahLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahLabel">Tambah Karyawan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('karyawan.post') }}" method="post">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nip" name="nip" required>
                            <label for="nip" value="{{ old('nip') }}">NIP</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nama" name="nama" required>
                            <label for="nama" value="{{ old('nama') }}">Nama</label>
                        </div>
                        <div class="form-floating mb-3">
                            <select class="form-select" id="jabatan" name="jabatan" required>
                                <option value="" selected disabled>Pilih Jabatan</option>
                                @foreach (JabatanEnum::getArrayExceptDirektur() as $key => $value)
                                    <option value="{{ $value }}" @selected(old('jabatan') == $value)>{{ $key }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="jabatan">Jabatan</label>
                        </div>
                        {{-- password --}}
                        <div class="form-floating mb-3">
                            <input type="password" class="form-control" id="password" name="password" required>
                            <label for="password">Password</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal delete karyawan --}}
    <div class="modal fade" id="modalDelete" tabindex="-1" aria-labelledby="deleteLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="formDelete">
                    @csrf
                    @method('delete')
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteLabel">Hapus Karyawan</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body pb-0">
                        <p>Apakah anda yakin ingin menghapus data karyawan ini?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal edit karyawan --}}
    <div class="modal fade" id="modalEdit" tabindex="-1" aria-labelledby="editLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editLabel">Edit Karyawan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" method="post" id="formEdit">
                    @csrf
                    @method('put')
                    <div class="modal-body pb-0">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nip" name="nip" required>
                            <label for="nip">NIP</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nama" name="nama" required>
                            <label for="nama">Nama</label>
                        </div>
                        <div class="form-floating mb-3">
                            <select class="form-select" id="jabatan" name="jabatan" required>
                                <option value="" selected disabled>Pilih Jabatan</option>
                                @foreach (JabatanEnum::getArrayExceptDirektur() as $key => $value)
                                    <option value="{{ $value }}">{{ $key }}
                                    </option>
                                @endforeach
                            </select>
                            <label for="jabatan">Jabatan</label>
                        </div>
                        {{-- password --}}
                        <div class="form-floating mb-3">
                            <input type="password" class="form-control" id="password" name="password">
                            <label for="password">Password</label>
                            <small class="text-muted">Kosongkan jika tidak ingin mengubah password</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#modalDelete').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var modal = $(this)
                $('#formDelete').attr('action', '/karyawan/' + id)
            })

            $('#modalEdit').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var nip = button.data('nip')
                var nama = button.data('nama')
                var jabatan = button.data('jabatan')
                var modal = $(this)
                $('#formEdit').attr('action', '/karyawan/' + id)
                $('#formEdit #nip').val(nip)
                $('#formEdit #nama').val(nama)
                $('#formEdit #jabatan').val(jabatan).change()
            })
        })
    </script>
    <script>
        $(document).ready(function() {
            $('#datatable-karyawan').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: "{{ route('karyawan') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        width: '30'
                    },
                    {
                        data: 'nip',
                        name: 'nip'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'jabatan',
                        name: 'jabatan',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        width: '120'
                    }
                ]
            });
        });
    </script>
@endpush
