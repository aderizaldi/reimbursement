@extends('user.layouts.main')
@use('\App\Enums\JabatanEnum')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">Pengajuan Reimbursement</h5>
                        <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" data-bs-toggle="modal"
                            data-bs-target="#modalTambah"><span class="tf-icons mdi mdi-plus"></span> Ajukan</button>
                    </div>
                    <div class="card-datatable table-responsive pt-0">
                        <table id="datatable-pengajuan-reimbursement" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Deskripsi</th>
                                    <th>Bukti</th>
                                    <th>Status Direktur</th>
                                    <th>Status Finance</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal tambah pengajuan reimbursement --}}
    <div class="modal fade" id="modalTambah" tabindex="-1" aria-labelledby="tambahLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tambahLabel">Tambah Pengajuan Reimbursement</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('pengajuan-reimbursement.post') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="nama" name="nama" required>
                            <label for="nama" value="{{ old('nama') }}">Nama</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="date" class="form-control" id="tanggal" name="tanggal" required>
                            <label for="tanggal" value="{{ old('tanggal') }}">Tanggal</label>
                        </div>
                        <div class="form-floating mb-3">
                            <textarea class="form-control h-px-100" id="deskripsi" name="deskripsi"></textarea>
                            <label for="deskripsi" value="{{ old('deskripsi') }}">Deskripsi (optional)</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="file" class="form-control" id="bukti" name="bukti" required>
                            <label for="bukti">Bukti</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Ajukan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#datatable-pengajuan-reimbursement').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: "{{ route('pengajuan-reimbursement') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        width: '30'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi',
                    },
                    {
                        data: 'bukti',
                        name: 'bukti',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'status_direktur',
                        name: 'status_direktur',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'status_finance',
                        name: 'status_finance',
                        orderable: false,
                        searchable: false,
                    }
                ],
            });
        });
    </script>
@endpush
