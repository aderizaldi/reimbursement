@extends('user.layouts.main')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <div class="col-12">
                <div class="card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-body">
                                <h4 class="card-title display-6 text-truncate mb-2 lh-sm">Selamat Datang
                                    {{ auth()->user()->nama }}! 👋</h4>
                                <p class="mb-0 mt-0">Selamat datang pada aplikasi reimbursement</p>
                            </div>
                        </div>
                        <div class="col-md-4 position-relative text-center">
                            <img src="{{ url('assets/img/illustrations/illustration-john.png') }}"
                                class="card-img-position bottom-0 w-px-300" alt="View Profile">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
