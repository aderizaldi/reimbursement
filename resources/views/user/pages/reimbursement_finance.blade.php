@extends('user.layouts.main')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row gy-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h5 class="card-title fs-5 p-0 m-0">Pengajuan Reimbursement</h5>
                    </div>
                    <div class="card-datatable table-responsive pt-0">
                        <table id="datatable-reimbursement-finance" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pengaju</th>
                                    <th>Nama</th>
                                    <th>Tanggal</th>
                                    <th>Deskripsi</th>
                                    <th>Bukti</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal terima reimbursement --}}
    <div class="modal fade" id="terimaModal" tabindex="-1" aria-labelledby="terimaLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="terimaLabel">Terima Reimbursement</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" method="post">
                    @csrf
                    @method('put')
                    <div class="modal-body pb-0">
                        <p>Apakah anda yakin ingin menerima permohonan reimbursement ini?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Terima</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- modal tolak reimbursement --}}
    <div class="modal fade" id="tolakModal" tabindex="-1" aria-labelledby="tolakLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tolakLabel">Tolak Reimbursement</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="" method="post">
                    @csrf
                    @method('put')
                    <div class="modal-body pb-0">
                        <p>Apakah anda yakin ingin menolak permohonan reimbursement ini?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-danger">Tolak</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#terimaModal').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var modal = $(this)
                modal.find('form').attr('action', '/reimbursement/terima/' + id)
            })

            $('#tolakModal').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget)
                var id = button.data('id')
                var modal = $(this)
                modal.find('form').attr('action', '/reimbursement/tolak/' + id)
            })
        })
    </script>
    <script>
        $(document).ready(function() {
            $('#datatable-reimbursement-finance').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                ajax: "{{ route('reimbursement-finance') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        width: '30'
                    },
                    {
                        data: 'pengaju',
                        name: 'pengaju',
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi',
                    },
                    {
                        data: 'bukti',
                        name: 'bukti',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false,
                        width: '120'
                    },
                ],
            });
        });
    </script>
@endpush
